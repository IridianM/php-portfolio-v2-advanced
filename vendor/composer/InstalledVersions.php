<?php

namespace Composer;

use Composer\Semver\VersionParser;






class InstalledVersions
{
private static $installed = array (
  'root' => 
  array (
    'pretty_version' => 'No version set (parsed as 1.0.0)',
    'version' => '1.0.0.0',
    'aliases' => 
    array (
    ),
    'reference' => NULL,
    'name' => '__root__',
  ),
  'versions' => 
  array (
    '__root__' => 
    array (
      'pretty_version' => 'No version set (parsed as 1.0.0)',
      'version' => '1.0.0.0',
      'aliases' => 
      array (
      ),
      'reference' => NULL,
    ),
    'aura/router' => 
    array (
      'pretty_version' => '3.1.0',
      'version' => '3.1.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '52507bc813c92511dbcacc7463f163ef5149ad38',
    ),
    'cakephp/cache' => 
    array (
      'pretty_version' => '3.8.12',
      'version' => '3.8.12.0',
      'aliases' => 
      array (
      ),
      'reference' => '09b2a1c4929e134456d30c61d9dbf6e10ace8436',
    ),
    'cakephp/collection' => 
    array (
      'pretty_version' => '3.8.12',
      'version' => '3.8.12.0',
      'aliases' => 
      array (
      ),
      'reference' => '013e9d02552c56d737a6bff91e59544c735efed7',
    ),
    'cakephp/core' => 
    array (
      'pretty_version' => '3.8.12',
      'version' => '3.8.12.0',
      'aliases' => 
      array (
      ),
      'reference' => '16249fa6771663e6649cbdb832f7ff25bf568b84',
    ),
    'cakephp/database' => 
    array (
      'pretty_version' => '3.8.12',
      'version' => '3.8.12.0',
      'aliases' => 
      array (
      ),
      'reference' => '489591545fd223d6cdbf022049f7c73ede0cb4fa',
    ),
    'cakephp/datasource' => 
    array (
      'pretty_version' => '3.8.12',
      'version' => '3.8.12.0',
      'aliases' => 
      array (
      ),
      'reference' => 'adc983f7c4d99c4361e3a8e4c0db7cdde26ee180',
    ),
    'cakephp/log' => 
    array (
      'pretty_version' => '3.8.12',
      'version' => '3.8.12.0',
      'aliases' => 
      array (
      ),
      'reference' => '2dae5b32430ca89d66c73bd9d568fcf68736eae5',
    ),
    'cakephp/utility' => 
    array (
      'pretty_version' => '3.8.12',
      'version' => '3.8.12.0',
      'aliases' => 
      array (
      ),
      'reference' => '9f121defbff4b5f3691b33de8cb203b8923fc2a4',
    ),
    'doctrine/inflector' => 
    array (
      'pretty_version' => '1.3.1',
      'version' => '1.3.1.0',
      'aliases' => 
      array (
      ),
      'reference' => 'ec3a55242203ffa6a4b27c58176da97ff0a7aec1',
    ),
    'illuminate/container' => 
    array (
      'pretty_version' => 'v5.8.36',
      'version' => '5.8.36.0',
      'aliases' => 
      array (
      ),
      'reference' => 'b42e5ef939144b77f78130918da0ce2d9ee16574',
    ),
    'illuminate/contracts' => 
    array (
      'pretty_version' => 'v5.8.36',
      'version' => '5.8.36.0',
      'aliases' => 
      array (
      ),
      'reference' => '00fc6afee788fa07c311b0650ad276585f8aef96',
    ),
    'illuminate/database' => 
    array (
      'pretty_version' => 'v5.8.36',
      'version' => '5.8.36.0',
      'aliases' => 
      array (
      ),
      'reference' => 'ac9ae2d82b8a6137400f17b3eea258be3518daa9',
    ),
    'illuminate/support' => 
    array (
      'pretty_version' => 'v5.8.36',
      'version' => '5.8.36.0',
      'aliases' => 
      array (
      ),
      'reference' => 'df4af6a32908f1d89d74348624b57e3233eea247',
    ),
    'jeremeamia/superclosure' => 
    array (
      'pretty_version' => '2.4.0',
      'version' => '2.4.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '5707d5821b30b9a07acfb4d76949784aaa0e9ce9',
    ),
    'middlewares/aura-router' => 
    array (
      'pretty_version' => 'v1.1.0',
      'version' => '1.1.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '4e825a421a9857c1a344109bd41019dbb8280e76',
    ),
    'middlewares/utils' => 
    array (
      'pretty_version' => 'v2.2.0',
      'version' => '2.2.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '7dc49454b4fbf249226023c7b77658b6068abfbc',
    ),
    'monolog/monolog' => 
    array (
      'pretty_version' => '1.25.4',
      'version' => '1.25.4.0',
      'aliases' => 
      array (
      ),
      'reference' => '3022efff205e2448b560c833c6fbbf91c3139168',
    ),
    'nesbot/carbon' => 
    array (
      'pretty_version' => '2.33.0',
      'version' => '2.33.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '4d93cb95a80d9ffbff4018fe58ae3b7dd7f4b99b',
    ),
    'nikic/php-parser' => 
    array (
      'pretty_version' => 'v4.4.0',
      'version' => '4.4.0.0',
      'aliases' => 
      array (
      ),
      'reference' => 'bd43ec7152eaaab3bd8c6d0aa95ceeb1df8ee120',
    ),
    'php-di/invoker' => 
    array (
      'pretty_version' => '2.0.0',
      'version' => '2.0.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '540c27c86f663e20fe39a24cd72fa76cdb21d41a',
    ),
    'php-di/php-di' => 
    array (
      'pretty_version' => '6.0.11',
      'version' => '6.0.11.0',
      'aliases' => 
      array (
      ),
      'reference' => '9bdcc2f41f5fb700ddd01bc4fa8d5bd7b3f94620',
    ),
    'php-di/phpdoc-reader' => 
    array (
      'pretty_version' => '2.1.1',
      'version' => '2.1.1.0',
      'aliases' => 
      array (
      ),
      'reference' => '15678f7451c020226807f520efb867ad26fbbfcf',
    ),
    'psr/container' => 
    array (
      'pretty_version' => '1.0.0',
      'version' => '1.0.0.0',
      'aliases' => 
      array (
      ),
      'reference' => 'b7ce3b176482dbbc1245ebf52b181af44c2cf55f',
    ),
    'psr/container-implementation' => 
    array (
      'provided' => 
      array (
        0 => '^1.0',
      ),
    ),
    'psr/http-factory' => 
    array (
      'pretty_version' => '1.0.1',
      'version' => '1.0.1.0',
      'aliases' => 
      array (
      ),
      'reference' => '12ac7fcd07e5b077433f5f2bee95b3a771bf61be',
    ),
    'psr/http-factory-implementation' => 
    array (
      'provided' => 
      array (
        0 => '1.0',
      ),
    ),
    'psr/http-message' => 
    array (
      'pretty_version' => '1.0.1',
      'version' => '1.0.1.0',
      'aliases' => 
      array (
      ),
      'reference' => 'f6561bf28d520154e4b0ec72be95418abe6d9363',
    ),
    'psr/http-message-implementation' => 
    array (
      'provided' => 
      array (
        0 => '1.0',
      ),
    ),
    'psr/http-server-handler' => 
    array (
      'pretty_version' => '1.0.1',
      'version' => '1.0.1.0',
      'aliases' => 
      array (
      ),
      'reference' => 'aff2f80e33b7f026ec96bb42f63242dc50ffcae7',
      'provided' => 
      array (
        0 => '^1.0',
      ),
    ),
    'psr/http-server-middleware' => 
    array (
      'pretty_version' => '1.0.1',
      'version' => '1.0.1.0',
      'aliases' => 
      array (
      ),
      'reference' => '2296f45510945530b9dceb8bcedb5cb84d40c5f5',
    ),
    'psr/log' => 
    array (
      'pretty_version' => '1.1.3',
      'version' => '1.1.3.0',
      'aliases' => 
      array (
      ),
      'reference' => '0f73288fd15629204f9d42b7055f72dacbe811fc',
    ),
    'psr/log-implementation' => 
    array (
      'provided' => 
      array (
        0 => '1.0.0',
        1 => '1.0',
      ),
    ),
    'psr/simple-cache' => 
    array (
      'pretty_version' => '1.0.1',
      'version' => '1.0.1.0',
      'aliases' => 
      array (
      ),
      'reference' => '408d5eafb83c57f6365a3ca330ff23aa4a5fa39b',
    ),
    'respect/validation' => 
    array (
      'pretty_version' => '1.1.31',
      'version' => '1.1.31.0',
      'aliases' => 
      array (
      ),
      'reference' => '45d109fc830644fecc1145200d6351ce4f2769d0',
    ),
    'robmorgan/phinx' => 
    array (
      'pretty_version' => '0.11.7',
      'version' => '0.11.7.0',
      'aliases' => 
      array (
      ),
      'reference' => '3cdde73e0c33c410e076108b3e1603fabb5b330d',
    ),
    'symfony/config' => 
    array (
      'pretty_version' => 'v4.4.9',
      'version' => '4.4.9.0',
      'aliases' => 
      array (
      ),
      'reference' => '395f6e09e1dc6ac9c1a5eea3b7f44f7a820a5504',
    ),
    'symfony/console' => 
    array (
      'pretty_version' => 'v4.4.9',
      'version' => '4.4.9.0',
      'aliases' => 
      array (
      ),
      'reference' => '326b064d804043005526f5a0494cfb49edb59bb0',
    ),
    'symfony/filesystem' => 
    array (
      'pretty_version' => 'v4.4.9',
      'version' => '4.4.9.0',
      'aliases' => 
      array (
      ),
      'reference' => 'b27f491309db5757816db672b256ea2e03677d30',
    ),
    'symfony/polyfill-ctype' => 
    array (
      'pretty_version' => 'v1.17.0',
      'version' => '1.17.0.0',
      'aliases' => 
      array (
      ),
      'reference' => 'e94c8b1bbe2bc77507a1056cdb06451c75b427f9',
    ),
    'symfony/polyfill-mbstring' => 
    array (
      'pretty_version' => 'v1.16.0',
      'version' => '1.16.0.0',
      'aliases' => 
      array (
      ),
      'reference' => 'a54881ec0ab3b2005c406aed0023c062879031e7',
    ),
    'symfony/polyfill-php56' => 
    array (
      'pretty_version' => 'v1.17.0',
      'version' => '1.17.0.0',
      'aliases' => 
      array (
      ),
      'reference' => 'e3c8c138280cdfe4b81488441555583aa1984e23',
    ),
    'symfony/polyfill-php73' => 
    array (
      'pretty_version' => 'v1.17.0',
      'version' => '1.17.0.0',
      'aliases' => 
      array (
      ),
      'reference' => 'a760d8964ff79ab9bf057613a5808284ec852ccc',
    ),
    'symfony/polyfill-php80' => 
    array (
      'pretty_version' => 'v1.17.0',
      'version' => '1.17.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '5e30b2799bc1ad68f7feb62b60a73743589438dd',
    ),
    'symfony/polyfill-util' => 
    array (
      'pretty_version' => 'v1.17.0',
      'version' => '1.17.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '4afb4110fc037752cf0ce9869f9ab8162c4e20d7',
    ),
    'symfony/service-contracts' => 
    array (
      'pretty_version' => 'v1.1.8',
      'version' => '1.1.8.0',
      'aliases' => 
      array (
      ),
      'reference' => 'ffc7f5692092df31515df2a5ecf3b7302b3ddacf',
    ),
    'symfony/translation' => 
    array (
      'pretty_version' => 'v4.4.8',
      'version' => '4.4.8.0',
      'aliases' => 
      array (
      ),
      'reference' => '8272bbd2b7e220ef812eba2a2b30068a5c64b191',
    ),
    'symfony/translation-contracts' => 
    array (
      'pretty_version' => 'v1.1.7',
      'version' => '1.1.7.0',
      'aliases' => 
      array (
      ),
      'reference' => '364518c132c95642e530d9b2d217acbc2ccac3e6',
    ),
    'symfony/translation-implementation' => 
    array (
      'provided' => 
      array (
        0 => '1.0',
      ),
    ),
    'symfony/yaml' => 
    array (
      'pretty_version' => 'v4.4.9',
      'version' => '4.4.9.0',
      'aliases' => 
      array (
      ),
      'reference' => 'c2d2cc66e892322cfcc03f8f12f8340dbd7a3f8a',
    ),
    'twig/twig' => 
    array (
      'pretty_version' => 'v2.12.5',
      'version' => '2.12.5.0',
      'aliases' => 
      array (
      ),
      'reference' => '18772e0190734944277ee97a02a9a6c6555fcd94',
    ),
    'woohoolabs/harmony' => 
    array (
      'pretty_version' => '6.1.0',
      'version' => '6.1.0.0',
      'aliases' => 
      array (
      ),
      'reference' => 'a7f918abb081ff56bf2078eaa3385038f3b76838',
    ),
    'zendframework/zend-diactoros' => 
    array (
      'pretty_version' => '2.2.1',
      'version' => '2.2.1.0',
      'aliases' => 
      array (
      ),
      'reference' => 'de5847b068362a88684a55b0dbb40d85986cfa52',
    ),
    'zendframework/zend-httphandlerrunner' => 
    array (
      'pretty_version' => '1.1.0',
      'version' => '1.1.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '75fb12751fe9d6e392cce1ee0d687dacae2db787',
    ),
  ),
);







public static function getInstalledPackages()
{
return array_keys(self::$installed['versions']);
}









public static function isInstalled($packageName)
{
return isset(self::$installed['versions'][$packageName]);
}














public static function satisfies(VersionParser $parser, $packageName, $constraint)
{
$constraint = $parser->parseConstraints($constraint);
$provided = $parser->parseConstraints(self::getVersionRanges($packageName));

return $provided->matches($constraint);
}










public static function getVersionRanges($packageName)
{
if (!isset(self::$installed['versions'][$packageName])) {
throw new \OutOfBoundsException('Package "' . $packageName . '" is not installed');
}

$ranges = array();
if (isset(self::$installed['versions'][$packageName]['pretty_version'])) {
$ranges[] = self::$installed['versions'][$packageName]['pretty_version'];
}
if (array_key_exists('aliases', self::$installed['versions'][$packageName])) {
$ranges = array_merge($ranges, self::$installed['versions'][$packageName]['aliases']);
}
if (array_key_exists('replaced', self::$installed['versions'][$packageName])) {
$ranges = array_merge($ranges, self::$installed['versions'][$packageName]['replaced']);
}
if (array_key_exists('provided', self::$installed['versions'][$packageName])) {
$ranges = array_merge($ranges, self::$installed['versions'][$packageName]['provided']);
}

return implode(' || ', $ranges);
}





public static function getVersion($packageName)
{
if (!isset(self::$installed['versions'][$packageName])) {
throw new \OutOfBoundsException('Package "' . $packageName . '" is not installed');
}

if (!isset(self::$installed['versions'][$packageName]['version'])) {
return null;
}

return self::$installed['versions'][$packageName]['version'];
}





public static function getPrettyVersion($packageName)
{
if (!isset(self::$installed['versions'][$packageName])) {
throw new \OutOfBoundsException('Package "' . $packageName . '" is not installed');
}

if (!isset(self::$installed['versions'][$packageName]['pretty_version'])) {
return null;
}

return self::$installed['versions'][$packageName]['pretty_version'];
}





public static function getReference($packageName)
{
if (!isset(self::$installed['versions'][$packageName])) {
throw new \OutOfBoundsException('Package "' . $packageName . '" is not installed');
}

if (!isset(self::$installed['versions'][$packageName]['reference'])) {
return null;
}

return self::$installed['versions'][$packageName]['reference'];
}





public static function getRootPackage()
{
return self::$installed['root'];
}







public static function getRawData()
{
return self::$installed;
}



















public static function reload($data)
{
self::$installed = $data;
}
}
