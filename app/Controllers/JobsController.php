<?php
namespace App\Controllers;

use Zend\Diactoros\Response\RedirectResponse;
use Zend\Diactoros\ServerRequest;
use App\Models\Job;
use Respect\Validation\Validator as v;
use App\Services\JobService;

class JobsController extends BaseController {
    private $jobService;

    public function __construct(JobService $jobService)
    {
        parent::__construct();//para heredar twig
        $this->jobService = $jobService;
    }

    public function indexAction () //Muestra el index para eliminar Jobs.
    {
        //$jobs = Job::withTrashed()->get(); //Muestra registros "Eliminados".
        $jobs = Job::all();
        return $this->renderHTML('jobs/index.twig', compact('jobs'));
    }

    public function deleteAction (ServerRequest $request) //Muestra lo que pasa después de hacer el borrado.
    {
        $params = $request->getQueryParams();
        $this->$jobService->deleteJob($params['id']);

        $job->delete();

        return new RedirectResponse('/admin/jobs');
    }



    public function getAddJobAction ($request) {
        $responseMessage = null;

        if($request->getMethod() == 'POST') {
            $postData = $request->getParsedBody();

            $jobValidator =v::key('title', v::stringType()->notEmpty())
                  ->key('description', v::stringType()->notEmpty());
                  
            $fileValidator=v::attribute('file',v::image()->size(null,'1MB'));

                  try{
                      $jobValidator->assert($postData);
                      $postData = $request->getParsedBody();


                      $files = $request->getUploadedFiles();
                      $logo = $files['logo'];
                      $fileValidator->assert($logo);


                      if ($logo->getError() == UPLOAD_ERR_OK) {
                        $fileName = $logo->getClientFileName();
                        $logo->moveTo("uploads/$fileName");

                      }

                      $job = new Job();   //Creamos una nueva instancia de job
                      $job->title = $postData['title'];
                      $job->description = $postData['description'];
                      $job->image = $fileName;
                      $job->save();

                      $responseMessage = 'Saved.';
                  }
                  catch (\Exception $e) {
                    $responseMessage = $e->getMessage();
                  }
        }
        return $this->renderHTML('addJob.twig', [
            'responseMessage' => $responseMessage
        ]);
    }
}