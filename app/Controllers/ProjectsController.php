<?php
namespace App\Controllers;

use App\Models\Project;
use Respect\Validation\Validator as v;

class ProjectsController extends BaseController {

    public function getAddProjectAction ($request) {
        $responseMessageProject = null;

        if($request->getMethod() == 'POST') {
            $postData = $request->getParsedBody();

            $projectValidator =v::key('title', v::stringType()->notEmpty())
                  ->key('description', v::stringType()->notEmpty());

                  try
                  {
                    $projectValidator->assert($postData);
                    $postData = $request->getParsedBody();


                    $files = $request->getUploadedFiles();
                    $logo = $files['logo'];

                      if ($logo->getError() == UPLOAD_ERR_OK) {
                        $fileName = $logo->getClientFileName();
                        $logo->moveTo("uploads/projects/$fileName");
                      }


                    $project = new Project();   //Creamos una nueva instancia de job
                    $project->title = $postData['title'];
                    $project->description = $postData['description'];
                    $project->filename = $fileName;
                    $project->save();

                    $responseMessageProject = 'Saved.';
                  }
                  catch (\Exception $e) {
                    $responseMessageProject = $e->getMessage();
                  }
            
        }
        return $this->renderHTML('addProject.twig', [
            'responseMessageProject' => $responseMessageProject
        ]);
    }
}