<?php

namespace App\Controllers;

use App\Models\User;

use Respect\Validation\Validator as valid;

class UsersController extends BaseController{

    public function getAddUserAction($request){

        $responseMessage = null;
        $findMessages = null;

        if($request->getMethod()=='POST'){

            $postData = $request->getParsedBody();

            //Se valida que tanto el nombre como la contraseña no tengan espacios en blanco, esten vacías y sean de entre 1 y 16 caracteres
            $userValidator = valid::key('name', valid::stringType()->notEmpty()->noWhitespace()->length(1,16))
            ->key('password', valid::stringType()->notEmpty()->noWhitespace()->length(1,16))
            ->key('passwordc', valid::stringType()->notEmpty()->noWhitespace()->length(1,16));

            try{

                $userValidator->assert($postData);
                $postData = $request->getParsedBody();
                
                //Se comprueba que la contraseña confirmada es la misma
                if($postData['password'] == $postData['passwordc']){
    
                    $user = new User();
                    $user->name = $postData['name'];
                    //Se usa password_hash para ocultar la contraseña
                    $hashed_password = password_hash($postData['password'], PASSWORD_DEFAULT);
                    $user->password = $hashed_password;
                    $user->save();
    
                    $responseMessage = 'User Registered';
                }else{
    
                    $responseMessage = 'Password is not the same';
                }
            }
            catch(\Exception $e){

                //Dependiendo el error al escribir la contraseña se muestran distintas alertas
                $findMessages = $e->findMessages([
                    'noWhitespace' => 'No se permiten espacios en blancos', 
                    'noEmpty' => 'Falta información por llenar', 
                    'length' => 'Tanto la contraseña como el nombre debe tener entre 1 y 16 caracteres'
                ]);
            }
        }

        return $this->renderHTML('addUser.twig',[

            //responseMessage es una cadena de texto, y findMessagges un array
            'responseMessage' => $responseMessage,
            'findMessages' => $findMessages
        ]);
    }
}