<?php

namespace App\Controllers;
use App\Models\{Job, Project, UserInfo};

class IndexController extends BaseController {

    public function indexAction () 
    {
        $jobs = Job::all(); //Nos va a traer todos los registros que encuentre
        $projects = Project::all();
        $userInfo = UserInfo::all();


        // $limitMonths = 0;

        // $filterFunction = function($job) use ($limitMonths){
        //     return $job['months'] >= $limitMonths;
        // };

        // $jobs = array_filter($jobs->toArray(), $filterFunction);
    
        //$name = 'Iridian';

        return  $this->renderHTML('index.twig', [
            //'name'          => $name,
            'jobs'          => $jobs,
            'projects'      => $projects,
            'userInfo'      => $userInfo
        ]);
    }  
}

?>