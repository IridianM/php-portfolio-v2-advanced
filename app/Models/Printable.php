<?php
namespace App\Models; //Esto va a funcionar dentro de app\models.

interface Printable {
    public function getDescription ();
}