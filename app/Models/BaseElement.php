<?php
  namespace App\Models;
  require_once 'app/Models/Printable.php';
  

class BaseElement implements Printable{
    protected $title;
    public $description;
    public $visible;
    public $months;

    public function __construct($title, $description) {                             //
      $this->setTitle($title);
      $this->description = $description;
    }

    public function setTitle ($title) {
      if ($title == '') {                                       //Valida si es un cadena vacia.
        $this->title = 'N/A';
      }
      else 
      {
        $this->title = $title;
      } 
    }

    public function getTitle () {
        return $this->title;
    }

    public function getDurationAsString () {                    //Imprimir los meses trabajados.
      $years = floor($this->months / 12);                       //para los años
      $extraMonths = $this->months % 12;
  
      $newYears  = ($years > 0) ? "$years years" : "";
      $newMonths = ($extraMonths > 0)? "$extraMonths months" : "";
  
      return "$newYears $newMonths";
    }

    //Printable
    public function getDescription () {
      return $this->description;
    }
}

?>