<?php
namespace App\Models; //Esto va a funcionar dentro de app\models.

// use App\Traits\HasDefaultImage;
use  Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Job extends Model {
    // //Para usar el trait.
    // use HasDefaultImage;

    use SoftDeletes;


    protected $table = 'jobs';
    

    public function getDurationAsString () {                    //Imprimir los meses trabajados.
        $years = floor($this->months / 12);                       //para los años
        $extraMonths = $this->months % 12;
    
        $newYears  = ($years > 0) ? "$years years" : "";
        $newMonths = ($extraMonths > 0)? "$extraMonths months" : "";
    
        return "Trabaje durante $newYears y $newMonths";
    }

}

?>