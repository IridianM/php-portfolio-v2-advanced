<?php

namespace App\Services;
use App\Models\Job;

class JobService 
{
    public function deleteJob($id)
    {
        $job = Job::findOrFail($id); //también podriamos usarlo en vez de todo el if de abajo.
        $job->delete();

        // $job = Job::find($id); //también funciona así.
        // if(!$job){
        //     throw new \Exception('Job not Found');
        // }
        // $job->delete();
        
    }
}
 
?>