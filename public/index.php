<?php

    
    require_once '../vendor/autoload.php';

    session_start();

    if (getenv ('DEBUG') === 'true') {

        ini_set('display_error', 1);
        ini_set('display_starup_error', 1);
        error_reporting(E_ALL);
        
    }


    use Franzl\Middleware\Whoops\WhoopsMiddleware;
    use App\Middlewares\AuthenticationMiddleware;
    use Illuminate\Database\Capsule\Manager as Capsule;
    use Aura\Router\RouterContainer;
    use WoohooLabs\Harmony\Harmony;
    use WoohooLabs\Harmony\Middleware\DispatcherMiddleware;
    use WoohooLabs\Harmony\Middleware\LaminasEmitterMiddleware;
    use WoohooLabs\Harmony\Middleware\HttpHandlerRunnerMiddleware;
    use Zend\Diactoros\ServerRequestFactory;
    use Zend\Diactoros\Response;
    use Zend\HttpHandlerRunner\Emitter\SapiEmitter;
    use Monolog\Logger;
    use Monolog\Handler\StreamHandler;

    // create a log channel
    $log = new Logger('app');
    $log->pushHandler(new StreamHandler(__DIR__ . '/logs/app-log'. Logger::WARNING));

    $container = new DI\Container();
    $capsule = new Capsule;
    $capsule->addConnection([
        'driver'    => 'mysql',
        'host'      => 'localhost',
        'database'  => 'cursophp',
        'username'  => 'root',
        'password'  => 'root',
        'charset'   => 'utf8',
        'collation' => 'utf8_unicode_ci',
        'prefix'    => '',
    ]);

    $capsule->setAsGlobal();                      // Make this Capsule instance available globally via static methods... (optional)
    $capsule->bootEloquent();                     // Setup the Eloquent ORM... (optional; unless you've used setEventDispatcher())

    $request = Zend\Diactoros\ServerRequestFactory::fromGlobals( //Library zend diactoros.
        $_SERVER,
        $_GET,
        $_POST,
        $_COOKIE,
        $_FILES
    );


    $routerContainer = new RouterContainer();
    $map = $routerContainer->getMap();
    $map->get('index', '/', [
        'App\Controllers\IndexController',
        'indexAction'
    ]);
    $map->get('contact', '/contact', [
        'App\Controllers\ContactController',
        'indexAction'
    ]);
    $map->post('sendContact', '/contact/send', [
        'App\Controllers\ContactController',
        'sendAction'
    ]);
    $map->get('indexJobs', '/admin/jobs', [
        'App\Controllers\JobsController',
        'indexAction'
    ]);
    $map->get('deleteJobs', '/admin/jobs/{id}/delete', [
        'App\Controllers\JobsController',
        'deleteAction'
    ]);
    $map->get('addJobs', '/admin/jobs/add', [
        'App\Controllers\JobsController',
        'getAddJobAction'
    ]);
    $map->post('saveJobs', '/admin/jobs/add', [
        '\App\Controllers\JobsController',
        'getAddJobAction'
    ]);
    $map->get('addProjects', '/admin/projects/add', [
        'App\Controllers\ProjectsController',
        'getAddProjectAction'
    ]);
    $map->post('saveProjects', '/admin/projects/add', [
        '\App\Controllers\ProjectsController',
        'getAddProjectAction'
    ]);
    $map->get('addUser', '/admin/users/add', [
        'App\Controllers\UsersController',
        'getAddUserAction'
    ]);
    $map->post('saveUser', '/admin/users/add', [
        'App\Controllers\UsersController',
        'getAddUserAction'
    ]);
    $map->get('loginForm', '/login', [
        'App\Controllers\AuthController',
        'getLogin'
    ]);
    $map->get('logout', '/logout', [
        'App\Controllers\AuthController',
        'getLogout'
    ]);
    $map->post('auth', '/auth', [
        'App\Controllers\AuthController',
        'postLogin'
    ]);
    $map->get('admin', '/admin', [
        'App\Controllers\AdminController',
        'getIndex'
    ]);
    $map->get('admin.profile.changePassword', '/admin/profile/changePassword', [
        'App\Controllers\ProfileController',
        'changePassword'
    ]);
    $map->post('admin.profile.savePassword', '/admin/profile/savePassword', [
        'App\Controllers\ProfileController',
        'savePassword'
    ]);


    $matcher = $routerContainer->getMatcher();
    $route = $matcher->match($request);



    if (!$route)
    {
        header('Location: /login');
  		    exit;
    }
    else
     {
        try{
            $harmony = new Harmony($request, new Response());
        
            $harmony->addMiddleware(new HttpHandlerRunnerMiddleware(new SapiEmitter()));
            if (getenv('DEBUG') === "true") {
                $harmony->addMiddleware(new \Franzl\Middleware\Whoops\WhoopsMiddleware);
            }
            $harmony->addMiddleware(new Middlewares\AuraRouter($routerContainer))
                ->addMiddleware(new AuthenticationMiddleware())
                ->addMiddleware(new DispatcherMiddleware($container, 'request-handler'));
        
            $harmony();
        } catch (Exception $e) {
            $log->error($e->getMessage());
            $emitter = new SapiEmitter();
            $emitter->emit(new Response\EmptyResponse(500));
        } catch (Error $e) {
            $log->error($e->getMessage());
            $emitter = new SapiEmitter();
            $emitter->emit(new Response\EmptyResponse(500));
        }
        
    }

?>