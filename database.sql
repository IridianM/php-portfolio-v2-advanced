/*#####crear base de datos*/
create database cursophp

use cursophp;

CREATE TABLE`cursophp`.`jobs` (
  `id`INT NOT NULL AUTO_INCREMENT,
  `title`TEXT NULL,
  `description`TEXT NULL,
  `filename` VARCHAR (200) NULL,,
  `visible` TINYINT(1) NULL,
  `months`INT NULL,
  `created_at` DATETIME NOT NULL,
  `updated_at` DATETIME NOT NULL,
  PRIMARY KEY (`id`));
  
  CREATE TABLE`cursophp`.`projects` (
  `id`INT NOT NULL AUTO_INCREMENT,
  `title`TEXT NULL,
  `description`TEXT NULL,
  `filename` VARCHAR (200) NULL,
  `visible` TINYINT(1) NULL,
  `months`INT NULL,
  `created_at` DATETIME NOT NULL,
  `updated_at` DATETIME NOT NULL,
  PRIMARY KEY (`id`));
  
  
  CREATE TABLE users (
  `id_usuario` smallint auto_increment primary key,
  `name` VARCHAR (255) NOT NULL,
  `password` VARCHAR(255)  NOT NULL,
  `created_at` DATETIME NOT NULL,
  `updated_at` DATETIME NOT NULL,
  `mail` VARCHAR (150) DEFAULT NULL
  );


  CREATE TABLE messages (
  `id` smallint auto_increment primary key,
  `name` VARCHAR (255) NOT NULL,
  `email` VARCHAR (255) NOT NULL,
  `message` VARCHAR (255) NOT NULL,
  `sent` boolean NOT NULL,
  `created_at` DATETIME NOT NULL,
 `updated_at` DATETIME NOT NULL
  );
  
CREATE TABLE info_user (
`id_infousr` smallint auto_increment primary key,
`name` VARCHAR (255) NOT NULL,
`mail` VARCHAR (255) NOT NULL,
`profession` VARCHAR (255) NOT NULL,
`phone` VARCHAR (255) NOT NULL,
`blog` VARCHAR (255) NOT NULL,
`summary` VARCHAR (1500) NOT NULL,
 `created_at` DATETIME NOT NULL,
 `updated_at` DATETIME NOT NULL,
 `deleted_at` DATETIME  NULL
);

DROP TABLE info_user;

insert into `info_user` (`name`, `mail`, `profession`, `phone`, `blog`, `summary`, `created_at`, `updated_at`, `deleted_at`)  
values ("Iridian Carrera", "iridian.carrera@gmail.com", "Eng. Information Technology", "5512345678", "irinah.com", " I´m a WEB and software developer with 5 years of experience developing software solutions that solve customer needs. I'm looking to be part of a team of  developers software. I love to create and contribute in technology projects that help people to improve their processes and make it easier for them, I've recently taken differents courses about software development who help me to continue learn and put them into practice.  ", "2020-03-28 01:24:14", "2020-03-28 01:24:14", "2020-03-28 01:24:14");




CREATE TABLE backend (
`id_backend` smallint auto_increment primary key,
`name_technology` VARCHAR (255) NOT NULL,
 `created_at` DATETIME NOT NULL,
 `updated_at` DATETIME NOT NULL
);

/*########################## ALTER TABLES ####################################*/
/*ALTER TABLE `jobs` ADD `created_at` DATETIME NOT NULL AFTER `months`, ADD `updated_at` DATETIME NOT NULL AFTER `created_at`;*/

ALTER TABLE `jobs` ADD `deleted_at` DATETIME  NULL AFTER `updated_at`;
ALTER TABLE `jobs` MODIFY `deleted_at` DATETIME  NULL;

/*change column name */
/*
ALTER TABLE `users` ADD `mail` VARCHAR (150) DEFAULT NULL AFTER `password`;
ALTER TABLE `users` CHANGE `nombre` `name` VARCHAR (255) NOT NULL;
ALTER TABLE `users` CHANGE `password` `password` VARCHAR(255)  NOT NULL;
ALTER TABLE `users` ADD `created_at` DATETIME NOT NULL AFTER `mail`, ADD `updated_at` DATETIME NOT NULL AFTER `created_at`;
*/
/*########################## INSERTS TABLES ####################################*/
insert into `jobs` (`title`, `description`, `image`, `months`, `updated_at`, `created_at`)  
values ("Alumno de PHP en PLATZI", "curso platzi.", "image.jpg", 50, "2020-03-28 01:24:14", "2020-03-28 01:24:14");


insert into `jobs` (`title`, `description`, `image`, `months`, `updated_at`, `created_at`)  
values ("PATRICIO", "curso platzi.", "image.jpg", 15,  "2020-03-28 01:24:14", "2020-03-28 01:24:14");


insert into `jobs` (`title`, `description`, `image`, `months`, `updated_at`, `created_at`, `deleted_at`)  
values ("CALAMARDO", "curso platzi.", "image.jpg", 10, "2020-03-28 01:24:14", "2020-03-28 01:24:14", "");


DELETE FROM users WHERE id_usuario > 0 AND id_usuario < 5;

/*########################## DELETE REGISTRES AND TABLES ####################################
DELETE FROM jobs WHERE id > 4 AND id < 50;
DELETE FROM projects WHERE id > 0 AND id < 14;
DELETE FROM users WHERE id_usuario > 0 AND id_usuario < 6;

DELETE FROM jobs WHERE id = 12;
DROP TABLE user_info;
*/

/*########################## SELECT CONSULTAS ####################################*/
SELECT * FROM jobs;
SELECT * FROM users;
SELECT * FROM projects;
SELECT * FROM messages;